package dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import modelo.Responsavel;

public class DAOResponsavel extends DAO<Responsavel>{
	
	public Responsavel findByName (String n){
		try {
			Query q = manager.createQuery("select r from Responsavel r where r.nome= '" + n +"'");
			return (Responsavel) q.getSingleResult();

		}catch(NoResultException e){			
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findAllNames() {
		try {
			Query q = manager.createQuery("SELECT r.nome FROM Responsavel r");
			return q.getResultList();

		}catch(NoResultException e){			
			return null;
		}
	}
}
