package modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Medico extends Pessoa{
	@Column(length=50, nullable=true)
	private String operadora;
	
	@Column(length=50, nullable=true, unique=true)
	private String crm;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
	@JoinColumn(name="medico_id")
	private List<Especialidade> especialidades = new ArrayList<Especialidade>();
	
	@OneToMany(mappedBy="medico", cascade=CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
	@JoinColumn(name="medico_id")
	private List<Atendimento> atendimentos = new ArrayList<Atendimento>();
	
	public Medico () {}
	
	public Medico(String nome, String cpf) {
		super(nome, cpf);
	}

	public String getOperadora() {
		return operadora;
	}

	public void setOperadora(String operadora) {
		this.operadora = operadora;
	}

	public String getCrm() {
		return crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}
	
	public List<Especialidade> getEspecialidades() {
		return especialidades;
	}
	
	public void adicionarEspecialidade(Especialidade e) {
		this.especialidades.add(e);
		e.setMedico(this);
	}

	public List<Atendimento> getAtendimentos() {
		return atendimentos;
	}

	public void adicionarAtendimento(Atendimento a) {
		this.atendimentos.add(a);
		a.setMedico(this);
	}

	@Override
	public String toString() {
		return super.toString() + " Medico [operadora: " +operadora+ ", crm: " +crm+ ", especialidades: " +especialidades+ 
				", atendimentos: " +atendimentos+ "] ";
	}
}
