package aplicacao;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import fachada.Fachada;

public class TelaQuantidadeMedicoAtendimento extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblNome;
	private JComboBox<String> cmbMedico;
	private JTextArea textArea;
	private JButton btnCriar;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaQuantidadeMedicoAtendimento frame = new TelaQuantidadeMedicoAtendimento();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public TelaQuantidadeMedicoAtendimento() {
		List<String> medicos = Fachada.listarNomesDosMedicos();
		
		setTitle("Listar Atendimentos");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 597, 278);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblNome = new JLabel("M�dicos: ");
		lblNome.setBounds(95, 217, 60, 20);
		contentPane.add(lblNome);
		
		cmbMedico = new JComboBox<String>();
		for(String m: medicos) cmbMedico.addItem(m); // adiciona os medicos no comboBox
		cmbMedico.setSelectedIndex(0);
		cmbMedico.setEditable(false);
		cmbMedico.setBounds(160, 218, 170, 20);
        contentPane.add(cmbMedico);

		btnCriar = new JButton("Listar");
		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					String medico = (String)cmbMedico.getSelectedItem();
					String result = Fachada.listarQuantidadeDeAtendimentoPorMedico(medico);
					
					textArea.setText(result);
				}
				catch(Exception erro){
					JOptionPane.showMessageDialog(null, erro.getMessage());
				}
			}
		});
		btnCriar.setBounds(350, 216, 115, 23);
		contentPane.add(btnCriar);
		
		textArea = new JTextArea();
		JScrollPane scroll = new JScrollPane(textArea);
		scroll.setBounds(24, 11, 535, 194);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		contentPane.add(scroll);
	}
}
