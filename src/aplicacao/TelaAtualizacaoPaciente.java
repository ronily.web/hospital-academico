package aplicacao;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import fachada.Fachada;
import modelo.Paciente;

public class TelaAtualizacaoPaciente extends JFrame {
	List<String> pacientes = Fachada.listarNomesDosPacientes();
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField1, textField_2, textField_3;
	private JLabel lblNome, lblCpf, lblCrm, lblMedicos;
	private JComboBox<String> cmbPaciente;
	private JButton btnCriar;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaAtualizacaoPaciente frame = new TelaAtualizacaoPaciente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public TelaAtualizacaoPaciente() {
		try { 
			Paciente paci = Fachada.procurarPacientePorNome(pacientes.get(0));
			
			setTitle("Atualizar Paciente");
			setResizable(false);
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 200, 280, 230);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			lblMedicos = new JLabel("Pacientes");
			lblMedicos.setBounds(10, 14, 85, 14);
			contentPane.add(lblMedicos); 
			
			lblNome = new JLabel("Nome");
			lblNome.setBounds(10, 52, 85, 14);
			contentPane.add(lblNome);

			lblCpf = new JLabel("Cpf");
			lblCpf.setBounds(10, 83, 85, 14);
			contentPane.add(lblCpf);
			
			lblCrm = new JLabel("Codigo");
			lblCrm.setBounds(10, 114, 85, 14);
			contentPane.add(lblCrm);
			
			cmbPaciente = new JComboBox<String>();
			for(String m: pacientes) cmbPaciente.addItem(m); // adiciona os medicos no comboBox
			cmbPaciente.setSelectedIndex(0);
			cmbPaciente.setEditable(false);
			cmbPaciente.setBounds(100, 18, 150, 20);
	        contentPane.add(cmbPaciente);
			
			textField1 = new JTextField();
			textField1.setBounds(100, 49, 150, 20);
			contentPane.add(textField1);
			textField1.setColumns(10);
			textField1.setText(paci.getNome());

			textField_2 = new JTextField();
			textField_2.setBounds(100, 80, 150, 20);
			contentPane.add(textField_2);
			textField_2.setColumns(10);
			textField_2.setText(paci.getCpf());

			textField_3 = new JTextField();
			textField_3.setBounds(100, 111, 150, 20);
			contentPane.add(textField_3);
			textField_3.setColumns(10);
			textField_3.setText(paci.getCodigo());
		}
		catch(Exception erro) {}
		
		cmbPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Paciente p = Fachada.procurarPacientePorNome((String)cmbPaciente.getSelectedItem());
					
					textField1.setText(p.getNome());
					textField_2.setText(p.getCpf());
					textField_3.setText(p.getCodigo());
				}
				catch(Exception erro) { }
			}
		});

		btnCriar = new JButton("Atualizar");
		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					String nomeSelected = (String)cmbPaciente.getSelectedItem();
					String nome = textField1.getText();
					String cpf = textField_2.getText();
					String codigo = textField_3.getText();
					
					// persist object
					Fachada.atualizarPaciente(nomeSelected, nome, cpf, codigo);
					JOptionPane.showMessageDialog(null, " atualizado");

					//clear inputs
					textField1.setText("");
					textField_2.setText("");
					textField_3.setText("");
					pacientes = Fachada.listarNomesDosPacientes();
					textField1.requestFocus();
					
					cmbPaciente.removeAllItems();
					pacientes = Fachada.listarNomesDosPacientes();
					for(String m: pacientes) cmbPaciente.addItem(m);
				}
				catch(Exception erro){
					JOptionPane.showMessageDialog(null, "erro");
				}
			}
		});
		btnCriar.setBounds(100, 145, 150, 23);
		contentPane.add(btnCriar);
	}
}

