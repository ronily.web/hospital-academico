package modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Paciente extends Pessoa{
	@Column(nullable=true, unique=true)
	private String codigo;
	
	@ManyToMany(mappedBy="pacientes", cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	List<Responsavel> responsaveis = new ArrayList<Responsavel>();
	
	@OneToMany(mappedBy="paciente", cascade=CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
	List<Atendimento> atendimentos = new ArrayList<Atendimento>();
	
	@ElementCollection(fetch=FetchType.EAGER)
	List<String> enfermidades = new ArrayList<String>();
	
	public Paciente () {}
	
	public Paciente(String nome, String cpf) {
		super(nome, cpf);
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public List<Responsavel> getResponsaveis() {
		return responsaveis;
	}
	
	public void adicionarResponsavel(Responsavel r) {
		this.responsaveis.add(r);
		r.adicionarPacientes(this);
	}

	public List<Atendimento> getAtendimentos() {
		return atendimentos;
	}
	
	public void adicionarAtendimento(Atendimento a) {
		this.atendimentos.add(a);
		a.setPaciente(this);
	}
		
	public List<String> getEnfermidades() {
		return enfermidades;
	}

	public void adicionarEnfermidade(String e) {
		this.enfermidades.add(e);
	}
	
	@Override
	public String toString() {
		return super.toString() + " Paciente [codigo: " +codigo+ ", responsaveis: " +responsaveis+ ", atendimentos: " +atendimentos+ 
				", enfermidades: " +enfermidades+ "] ";
	}
}
