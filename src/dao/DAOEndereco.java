package dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import modelo.Endereco;

public class DAOEndereco extends DAO<Endereco>{

	@SuppressWarnings("unchecked")
	public List<String> findAllNames() {
		try {
			Query q = manager.createQuery("select distinct e.bairro from Endereco e");
			return q.getResultList();
	
		}catch(NoResultException e){			
			return null;
		}
	}
}
