package dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import modelo.Atendimento;

public class DAOAtendimento extends DAO<Atendimento>{
	
	@SuppressWarnings("unchecked")
	public List<Atendimento> findAllOrder() {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String data = formatter.format(new Date());

			Query q = manager.createQuery("select a from Atendimento a where a.dataAtend >= '" + data +"' ORDER BY a.dataAtend ASC");
			return q.getResultList();

		}catch(NoResultException e){			
			return null;
		}		
	}

}
