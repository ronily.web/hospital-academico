package aplicacao;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import fachada.Fachada;
import modelo.Medico;

public class TelaRemocaoMedico extends JFrame {
	List<String> medicos = Fachada.listarNomesDosMedicos();
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblMedicos;
	private JComboBox<String> cmbMedico;
	private JButton btnCriar;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaRemocaoMedico frame = new TelaRemocaoMedico();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public TelaRemocaoMedico() {
		try { 			
			setTitle("Apagar Medico");
			setResizable(false);
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 200, 280, 130);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			lblMedicos = new JLabel("Medicos");
			lblMedicos.setBounds(10, 18, 85, 14);
			contentPane.add(lblMedicos); 
			
			cmbMedico = new JComboBox<String>();
			for(String m: medicos) cmbMedico.addItem(m); // adiciona os medicos no comboBox
			cmbMedico.setSelectedIndex(0);
			cmbMedico.setEditable(false);
			cmbMedico.setBounds(100, 18, 150, 20);
	        contentPane.add(cmbMedico);
		}
		catch(Exception erro) {}

		btnCriar = new JButton("Remover");
		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					String nomeSelected = (String)cmbMedico.getSelectedItem();
					
					// persist object
					Medico medico = Fachada.apagarMedico(nomeSelected);
					JOptionPane.showMessageDialog(null, medico.getNome() + " apagado");
					
					cmbMedico.removeAllItems();
					medicos = Fachada.listarNomesDosMedicos();
					for(String m: medicos) cmbMedico.addItem(m);
				}
				catch(Exception erro){
					JOptionPane.showMessageDialog(null, "erro");
				}
			}
		});
		btnCriar.setBounds(100, 60, 150, 23);
		contentPane.add(btnCriar);
	}
}

