package modelo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Atendimento {
	@Id
	@Temporal(TemporalType.DATE)
	private Date dataAtend;
	
	@Id
	@ManyToOne
	private Medico medico;
	
	@Id
	@ManyToOne
	private Paciente paciente;
	
	public Atendimento () {}
	
	public Atendimento (Medico medico, Paciente paciente, Date date) {
		this.medico = medico;
		this.paciente = paciente;
		this.dataAtend = date;
	}

	public Date getDataAtend() {
		return dataAtend;
	}

	public void setDataAtend(Date dataAtend) {
		this.dataAtend = dataAtend;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	@Override
	public String toString() {
		return " Atendimento [Data: " +dataAtend+ "]";
	}
}
