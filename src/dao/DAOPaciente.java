package dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import modelo.Paciente;

public class DAOPaciente extends DAO<Paciente>{
	
	public Paciente findByCodigo (String n){
		try {
			Query q = manager.createQuery("SELECT p FROM Paciente p WHERE p.codigo= '" + n +"'");
			return (Paciente) q.getSingleResult();

		}catch(NoResultException e){			
			return null;
		}
	}
	
	public Paciente findByName (String nome){
		try {
			Query q = manager.createQuery("SELECT m FROM Paciente m WHERE m.nome= '" + nome +"'");
			return (Paciente) q.getSingleResult();

		}catch(NoResultException e){			
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findAllNames() {
		try {
			Query q = manager.createQuery("SELECT p.nome FROM Paciente p");
			return q.getResultList();

		}catch(NoResultException e){			
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Paciente> findOfNeighborhood(String bairro) {
		try {
			Query q = manager.createQuery("SELECT p FROM Paciente p join p.endereco e WHERE e.bairro = '" + bairro +"'");
			return q.getResultList();

		}catch(NoResultException e){			
			return null;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<Paciente> findNotResponsible() {
		try {
			Query q = manager.createQuery("SELECT p FROM Paciente p WHERE SIZE(p.responsaveis) = 0");
			return q.getResultList();

		}catch(NoResultException e){			
			return null;
		}		
	}
}
