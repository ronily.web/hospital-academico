package aplicacao;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import fachada.Fachada;
import modelo.Endereco;
import modelo.Paciente;
import modelo.Telefone;

public class TelaCadastroPaciente extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField1, textField_2, textField_3, textField_4, textField_5, textField_6, textField_7, textField_8, textField_9;
	private JLabel lblNome, lblCpf, lblCodigo, lblTelefone, lblEnfermidade, lblCidade, lblBairro, lblRua, lblNumero;
	private JButton btnCriar;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroPaciente frame = new TelaCadastroPaciente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public TelaCadastroPaciente() {
		setTitle("Cadastrar Paciente");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 200, 280, 370);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblNome = new JLabel("Nome");
		lblNome.setBounds(10, 14, 85, 14);
		contentPane.add(lblNome);

		lblCpf = new JLabel("Cpf");
		lblCpf.setBounds(10, 52, 85, 14);
		contentPane.add(lblCpf);
		
		lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(10, 82, 85, 14);
		contentPane.add(lblTelefone);
		
		lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(10, 114, 85, 14);
		contentPane.add(lblCodigo);
		
		lblEnfermidade = new JLabel("Enfermidade");
		lblEnfermidade.setBounds(10, 145, 85, 14);
		contentPane.add(lblEnfermidade);
		
		lblCidade = new JLabel("Cidade");
		lblCidade.setBounds(10, 176, 85, 14);
		contentPane.add(lblCidade);
		
		lblBairro = new JLabel("Bairro");
		lblBairro.setBounds(10, 207, 85, 14);
		contentPane.add(lblBairro);
		
		lblRua = new JLabel("Rua");
		lblRua.setBounds(10, 238, 85, 14);
		contentPane.add(lblRua);
		
		lblNumero = new JLabel("Numero");
		lblNumero.setBounds(10, 269, 85, 14);
		contentPane.add(lblNumero);
		
		textField1 = new JTextField();
		textField1.setBounds(100, 18, 150, 20);
		contentPane.add(textField1);
		textField1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(100, 49, 150, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(100, 80, 150, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(100, 111, 150, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(100, 142, 150, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		textField_6 = new JTextField();
		textField_6.setBounds(100, 173, 150, 20);
		contentPane.add(textField_6);
		textField_6.setColumns(10);
		
		textField_7 = new JTextField();
		textField_7.setBounds(100, 204, 150, 20);
		contentPane.add(textField_7);
		textField_7.setColumns(10);
		
		textField_8 = new JTextField();
		textField_8.setBounds(100, 235, 150, 20);
		contentPane.add(textField_8);
		textField_8.setColumns(10);
		
		textField_9 = new JTextField();
		textField_9.setBounds(100, 266, 150, 20);
		contentPane.add(textField_9);
		textField_9.setColumns(10);		

		btnCriar = new JButton("Cadastrar");
		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					String nome = textField1.getText();
					String cpf = textField_2.getText();
					String telefone = textField_3.getText();
					String codigo = textField_4.getText();
					String enfermidade = textField_5.getText();
					String cidade = textField_6.getText();
					String bairro = textField_7.getText();
					String rua = textField_8.getText();
					String numero = textField_9.getText();
					
					// phone
					ArrayList<Telefone> telefones = new ArrayList<Telefone>();
					telefones.add(new Telefone(telefone));
					
					ArrayList<String> enfermidades = new ArrayList<String>();
					enfermidades.add(enfermidade);
					
					// address
					Endereco end = new Endereco(cidade, bairro, rua);
					if(!numero.isEmpty()) end.setNumero(numero);
				
					// persist object
					Paciente p = Fachada.cadastrarPaciente(nome, cpf, codigo, end, enfermidades, telefones);
					JOptionPane.showMessageDialog(null, p.getNome() + " cadastrado");

					//clear inputs
					textField1.setText("");
					textField_2.setText("");
					textField_3.setText("");
					textField_4.setText("");
					textField_5.setText("");
					textField_6.setText("");
					textField_7.setText("");
					textField_8.setText("");
					textField_9.setText("");
					textField1.requestFocus();
				}
				catch(Exception erro){
					JOptionPane.showMessageDialog(null, erro.getMessage());
				}
			}
		});
		btnCriar.setBounds(72, 300, 150, 23);
		contentPane.add(btnCriar);
	}
}

