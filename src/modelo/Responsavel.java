package modelo;

import java.util.ArrayList;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
public class Responsavel extends Pessoa{
	@Column(length=50, nullable=true)
	private String parentesco;
	
	@ManyToMany
	List<Paciente> pacientes = new ArrayList<Paciente>();
	
	public Responsavel () {}
	
	public Responsavel(String nome, String cpf) {
		super(nome, cpf);
	}
	
	public Responsavel (String parentesco) {
		this.parentesco = parentesco;
	}
	
	public List<Paciente> getPacientes() {
		return this.pacientes;
	}
	
	public void adicionarPacientes(Paciente p) {
		this.pacientes.add(p);
	}

	public String getParentesco() {
		return parentesco;
	}

	public void setParentesco(String parentesco) {
		this.parentesco = parentesco;
	}

	@Override
	public String toString() {
		return super.toString() + " Responsavel [parentesco: " +parentesco+ "]";
	}
}
