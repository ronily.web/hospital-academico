package modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class Pessoa {
	@Id		
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;
	
	@Column(length=50, nullable=false)
	private String nome;
	
	@Column(length=11, nullable=false)
	private String cpf;
	
	@OneToMany(mappedBy="pessoa", cascade=CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
	private List<Telefone> telefones = new ArrayList<Telefone>();
	
	@OneToOne(mappedBy="pessoa", cascade=CascadeType.ALL)
	private Endereco endereco = new Endereco();

	//construtor vazio
	public Pessoa (){}
	
	public Pessoa (String nome, String cpf) {
		this.nome = nome;
		this.cpf = cpf;
	}
	
	public int getId() {
		return id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public Endereco getEndereco() {
		return endereco;
	}
	
	public void adicionarEndereco(Endereco endereco) {
		endereco.setPessoa(this);
		this.endereco = endereco;
	}
	
	public List<Telefone> getTelefones() {
		return telefones;
	}

	public void adicionarTelefone(Telefone t) {
		this.telefones.add(t);
		t.setPessoa(this);
	}

	@Override
	public String toString() {
		return "Pessoa [Nome: " +nome+ ", CPF: " +cpf+ ", Telefones: " +telefones+ ", Endereco: " +endereco+ "]";
	}
}
