package fachada;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dao.*;
import modelo.*;

public class Fachada {
	private static DAOAtendimento daoatendimento = new DAOAtendimento();
	private static DAOEndereco daoendereco = new DAOEndereco();
	private static DAOMedico daomedico = new DAOMedico();
	private static DAOPaciente daopaciente = new DAOPaciente();
	private static DAOResponsavel daoresposavel = new DAOResponsavel();

	public static void inicializar(){
		DAO.open();
	}

	public static void finalizar(){
		DAO.close();
	}

	public static Medico cadastrarMedico(
		String nome, 
		String cpf, 
		String crm,
		String operadora,
		Endereco e, 
		List<Especialidade> espec
	) throws  Exception {
		
		DAO.begin();			
		Medico m = daomedico.findByCrm(crm);
			
		if(m != null) {
			DAO.rollback();
			throw new Exception("Medico ja cadastrado:" + nome);
		}

		m = new Medico(nome, cpf);
		m.adicionarEndereco(e);
		m.setCrm(crm);
		m.setOperadora(operadora);
		
		for(Especialidade flag: espec) {
			m.adicionarEspecialidade(flag);
		}
			
		daomedico.create(m);		
		DAO.commit();
	
		return m;
	}
	
	public static Medico cadastrarMedico(
		String nome,  
		String cpf, 
		String crm,
		String operadora,
		Endereco e, 
		List<Especialidade> espec,
		List<Telefone> t
	) throws  Exception {
		
		DAO.begin();			
		Medico m = daomedico.findByCrm(crm);
			
		if(m != null) {
			DAO.rollback();
			throw new Exception("Medico ja cadastrado:" + nome);
		}

		m = new Medico(nome, cpf);
		m.adicionarEndereco(e);
		m.setCrm(crm);
		m.setOperadora(operadora);
			
		// adi��o de especialidades
		for(Especialidade flag: espec) {
			m.adicionarEspecialidade(flag);
		}
		
		// adi��o de telefones
		for(Telefone flag: t) {
			m.adicionarTelefone(flag);
		}
			
		daomedico.create(m);		
		DAO.commit();
	
		return m;
	}
	
	public static Medico atualizarMedico(
			String NomeSelected,
			String nome,  
			String cpf, 
			String crm,
			String operadora
	) throws Exception{
		
		DAO.begin();
		
		Medico m = daomedico.findByName(NomeSelected);
		
		m.setNome(nome);
		m.setCpf(cpf);
		m.setCrm(crm);
		m.setOperadora(operadora);	
		
		daomedico.update(m);
		DAO.commit();
		
		return m;
	}
	
	public static Medico apagarMedico(String nome) {
			
		DAO.begin();			
		Medico m = daomedico.findByName(nome);

		daomedico.delete(m);		
		DAO.commit();
		
		return m;
	}
	
	public static String listarMedicos() {
		List<Medico> aux = daomedico.readAll();
		String text = "\nListagem de medicos: ";
		
		if(aux.isEmpty()) {
			text += "n�o h� m�dicos cadastrados";
		}
		else {
			for(Medico m: aux) {
				text += "\nNome: " + m.getNome()
					+ ", CRM: " + m.getCrm()
					+ ", OPERADORA: " + m.getOperadora()
					+ m.getTelefones().toString()
					+ m.getEndereco().toString();
			}
		}
		
		return text;
	}
	
	public static String listarMedicosPorBairro(String bairro) {
		List<Medico> aux = daomedico.findOfNeighborhood(bairro);
		String text = "\nListagem de medicos: ";
		
		if(aux.isEmpty()) {
			text += "n�o h� m�dicos nesse bairro";
		}
		else {
			for(Medico m: aux) {
				text += "\nNome: " + m.getNome()
						+ ", CRM: " + m.getCrm()
						+ ", OPERADORA: " + m.getOperadora()
						+ ", TELEFONE: " + m.getTelefones().toString();
			}
		}
		
		return text;
	}
	
	public static String listarMedicosDeBairroIguais(String nome) {
		List<Medico> aux = daomedico.findOfNeighborhoodEqual(nome);
		String text = "\nListagem de medicos: ";
		
		if(aux.isEmpty()) {
			text += "n�o h� vizinhos de bairro";
		}
		else {
			for(Medico m: aux) {
				text += "\nNome: " + m.getNome()
						+ ", CRM: " + m.getCrm()
						+ ", OPERADORA: " + m.getOperadora()
						+ ", TELEFONE: " + m.getTelefones().toString();
			}
		}
		
		return text;
	}
	
	public static String listarMedicosSemAtendimentos() {
		List<Medico> aux = daomedico.findNotAttendance();
		String text = "\nListagem de medicos: ";
		
		if(aux.isEmpty()) {
			text += "n�o h� medicos cadastrados";
		}
		else {
			for(Medico m: aux) {
				text += "\nNome: " + m.getNome()
						+ ", CRM: " + m.getCrm()
						+ ", OPERADORA: " + m.getOperadora()
						+ ", TELEFONE: " + m.getTelefones().toString();
			}
		}
		
		return text;
	}
	
	public static String listarMedicosComVariosTelefones() {
		List<Medico> aux = daomedico.findDoctorMultiplesPhones();
		String text = "\nListagem de medicos: ";
		
		if(aux.isEmpty()) {
			text += "n�o h� medicos cadastrados";
		}
		else {
			for(Medico m: aux) {
				text += "\nNome: " + m.getNome()
						+ ", CRM: " + m.getCrm()
						+ ", OPERADORA: " + m.getOperadora()
						+ ", TELEFONE: " + m.getTelefones().toString();
			}
		}
		
		return text;
	}
	
	public static String listarQuantidadeDeAtendimentoPorMedico(String nome) {
		Long aux = daomedico.CountAttendanceOfDoctor(nome);
		String text = "\nQuantidade de atendimentos: " + aux;
		
		return text;
	}
	
	
	public static List<String> listarNomesDosMedicos() {
		return daomedico.findAllNames();
	}
	
	public static Medico procurarMedicoPorNome(String nome) throws  Exception {
		Medico m = daomedico.findByName(nome);
		
		if(m == null) {
			
		}
		
		return m;
	}	
	
	public static Paciente cadastrarPaciente(
		String nome, 
		String cpf, 
		String codigo, 
		Endereco e,
		ArrayList<String> enfer
	) throws  Exception {
		
		DAO.begin();			
		Paciente p = daopaciente.findByCodigo(codigo);
			
		if(p != null) {
			DAO.rollback();
			throw new Exception("Paciente ja cadastrado: " + nome);
		}

		p = new Paciente(nome, cpf);
		p.adicionarEndereco(e);
		p.setCodigo(codigo);
			
		daopaciente.create(p);		
		DAO.commit();
	
		return p;
	}
	
	public static Paciente cadastrarPaciente(
		String nome, 
		String cpf, 
		String codigo, 
		Endereco e,
		ArrayList<String> enfer,
		List<Telefone> t
	) throws  Exception {
		
		DAO.begin();			
		Paciente p = daopaciente.findByCodigo(codigo);
			
		if(p != null) {
			DAO.rollback();
			throw new Exception("Paciente ja cadastrado: " + nome);
		}

		p = new Paciente(nome, cpf);
		p.adicionarEndereco(e);
		p.setCodigo(codigo);
		
		for(String flag: enfer) {
			p.adicionarEnfermidade(flag);
		}
		
		for(Telefone flag: t) {
			p.adicionarTelefone(flag);
		}
			
		daopaciente.create(p);		
		DAO.commit();
	
		return p;
	}
	
	public static Paciente atualizarPaciente(
			String NomeSelected,
			String nome,  
			String cpf, 
			String codigo
	) throws Exception{
		
		DAO.begin();
		
		Paciente p = daopaciente.findByName(NomeSelected);
		
		p.setNome(nome);
		p.setCpf(cpf);
		p.setCodigo(codigo);
		
		daopaciente.update(p);
		DAO.commit();
		
		return p;
	}
	
	public static Paciente apagarPaciente(String nome) {
		
		DAO.begin();			
		Paciente m = daopaciente.findByName(nome);

		daopaciente.delete(m);		
		DAO.commit();
		
		return m;
	}
	
	public static String listarPacientesPorBairro(String bairro) {
		List<Paciente> aux = daopaciente.findOfNeighborhood(bairro);
		String text = "\nListagem de pacientes: ";
		
		if(aux.isEmpty()) {
			text += "n�o h� pacientes nesse bairro";
		}
		else {
			for(Paciente p: aux) {
				text += "\nNome: " + p.getNome()
					+ ", C�DIGO: " + p.getCodigo()
					+ ", ENFERMIDADEs: " + p.getEnfermidades().toString()
					+ ", TELEFONEs: " + p.getTelefones().toString();
			}
		}
		
		return text;
	}
	
	public static String listarPacientes() {
		List<Paciente> aux = daopaciente.readAll();
		String text = "\nListagem de pacientes: ";
		
		if(aux.isEmpty()) {
			text += "n�o h� pacientes nesse bairro";
		}
		else {
			for(Paciente p: aux) {
				text += "\nNome: " + p.getNome()
					+ ", C�DIGO: " + p.getCodigo()
					+ ", ENFERMIDADEs: " + p.getEnfermidades().toString()
					+ p.getTelefones().toString()
					+ p.getEndereco().toString();
			}
		}
		
		return text;
	}
	
	public static String listarPacientesSemResponsaveis() {
		List<Paciente> aux = daopaciente.findNotResponsible();
		String text = "\nListagem de pacientes: ";
		
		if(aux.isEmpty()) {
			text += "n�o h� pacientes cadastrados";
		}
		else {
			for(Paciente p: aux) {
				text += "\nNome: " + p.getNome()
					+ ", C�DIGO: " + p.getCodigo()
					+ ", TELEFONEs: " + p.getTelefones().toString();
			}
		}
		
		return text;
	}
	
	public static List<String> listarNomesDosPacientes() {
		return daopaciente.findAllNames();
	}
	
	public static Paciente procurarPacientePorNome(String nome) throws  Exception {
		Paciente p = daopaciente.findByName(nome);
		
		if(p == null) {
			throw new Exception("Paciente n�o encontrado");
		}
		
		return p;
	}

	public static Responsavel cadastrarResponsavel(
		String nome,  
		String cpf, 
		Paciente p,
		String parentesco,
		Endereco e
	) throws  Exception {
		
		DAO.begin();			
		Responsavel r = daoresposavel.findByName(nome);
			
		if(r != null) {
			DAO.rollback();
			throw new Exception("Respons�vel ja cadastrado: " + nome);
		}

		r = new Responsavel(nome, cpf);
		r.adicionarEndereco(e);
		r.setParentesco(parentesco);
		r.adicionarPacientes(p);
			
		daoresposavel.create(r);		
		DAO.commit();
	
		return r;
	}
	
	public static Responsavel cadastrarResponsavel(
		String nome,  
		String cpf,  
		Paciente p,
		String parentesco, 
		Endereco e,
		List<Telefone> t
	) throws  Exception {
		
		DAO.begin();			
		Responsavel r = daoresposavel.findByName(nome);
			
		if(r != null) {
			DAO.rollback();
			throw new Exception("Respons�vel ja cadastrado: " + nome);
		}

		r = new Responsavel(nome, cpf);
		r.adicionarEndereco(e);
		r.setParentesco(parentesco);
		r.adicionarPacientes(p);
		
		// add phone
		for(Telefone flag: t) {
			r.adicionarTelefone(flag);
		}
			
		daoresposavel.create(r);		
		DAO.commit();
	
		return r;
	}
	
	public static Responsavel atualizarResponsavel(
			String NomeSelected,
			String nome,  
			String cpf
	) throws Exception{
		
		DAO.begin();
		
		Responsavel r = daoresposavel.findByName(NomeSelected);
		
		r.setNome(nome);
		r.setCpf(cpf);
		
		daoresposavel.update(r);
		DAO.commit();
		
		return r;
	}
	
	public static List<String> listarNomesDosResponsaveis() {
		return daomedico.findAllNames();
	}
	
	public static Responsavel procurarResponsavelPorNome(String nome) {
		Responsavel m = daoresposavel.findByName(nome);
		
		return m;
	}
	
	public static String listarResponsaveis() {
		List<Responsavel> aux = daoresposavel.readAll();
		String text = "\nListagem de respons�veis: ";
		
		if(aux.isEmpty()) {
			text += "n�o h� respons�veis cadastrados";
		}
		else {
			for(Responsavel r: aux) {
				text += "\nNome: " + r.getNome()
					+ ", Parentesco: " + r.getParentesco()
					+ r.getPacientes().toString()
					+ r.getTelefones().toString()
					+ r.getEndereco().toString();
			}
		}
		
		return text;
	}
	
	public static Responsavel apagarResponsavel(String nome){
		
		DAO.begin();			
		Responsavel r = daoresposavel.findByName(nome);

		daoresposavel.delete(r);		
		DAO.commit();
		
		return r;
	}
	
	public static Atendimento cadastrarAtendimento(Medico medico, Paciente paciente, Date date) throws  Exception {
		DAO.begin();			

		Atendimento a = new Atendimento(medico, paciente, date);
		
		daoatendimento.create(a);		
		DAO.commit();
	
		return a;
	}
	
	public static String listarAtendimentos() {
		List<Atendimento> aux = daoatendimento.readAll();
		String text = "\nListagem de atendimentos: ";
		
		if(aux.isEmpty()) {
			text += "n�o h� atendimentos cadastrados";
		}
		else {
			for(Atendimento a: aux) {
				text += "\n" + a;
			}
		}
		
		return text;
	}
	
	public static String listarAtendimentosOrder() {
		List<Atendimento> aux = daoatendimento.findAllOrder();
		String text = "\nListagem de atendimentos: ";
		SimpleDateFormat formatter = new SimpleDateFormat("DD/MM/YYYY");
		
		if(aux.isEmpty()) {
			text += "n�o h� atendimentos cadastrados";
		}
		else {
			for(Atendimento a: aux) {
				text += "\n" + formatter.format(a.getDataAtend())
					+ ", Medico: " + a.getMedico().getNome()
					+ ", Paciente: " + a.getPaciente().getNome();
			}
		}
		
		return text;
	}
	
	public static List<String> listarBairros() {
		return daoendereco.findAllNames();
	}
}
