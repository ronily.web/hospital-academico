package modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Endereco {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;
	
	@Column(length=100)
	private String cidade;
	
	@Column(length=100)
	private String bairro;
	
	@Column(length=100)
	private String rua;
	
	@Column(length=30, nullable=true)
	private String numero;
		
	@OneToOne
	private Pessoa pessoa;
	
	public Endereco() {}
	
	public Endereco(String cidade, String bairro, String rua) {
		this.cidade = cidade;
		this.bairro = bairro;
		this.rua = rua;
	}

	public int getId() {
		return id;
	}
	
	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	
	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	@Override
	public String toString() {
		return "Endereco [Cidade: " +cidade+ ", Bairro: " +bairro+ ", Rua: " +rua+ ",  numero: "+numero+"]";
	}
}
