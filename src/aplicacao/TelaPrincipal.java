package aplicacao;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import fachada.Fachada;
import modelo.Endereco;
import modelo.Especialidade;
import modelo.Medico;
import modelo.Paciente;
import modelo.Telefone;

public class TelaPrincipal {
	private JFrame frmPrincipal;
	private JMenuItem medCadastro, respCadastro, atendCadastro, paciCadastro;
	private JMenuItem medUpdate, medRemove, paciUpdate;
	private JMenuItem medConsult1, medConsult2, medConsult3, medConsult4, medConsult5, atendConsult1, paciConsult1, paciConsult2;
	private JMenuItem medConsult6, paciConsult3, respConsult1;
	private JMenu mnMedico, mnPaciente, mnResponsavel, mnAtendimento;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipal window = new TelaPrincipal();
					window.frmPrincipal.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public TelaPrincipal() {
		try {
			Fachada.inicializar();
			
			if(Fachada.procurarMedicoPorNome("Everaldo Morais") == null) {
				// carregamento das especialidades
				ArrayList<Especialidade> esp1 = new ArrayList<Especialidade>();
				esp1.add(new Especialidade("neurologia"));
				
				ArrayList<Especialidade> esp2 = new ArrayList<Especialidade>();
				esp2.add(new Especialidade("pl�stica"));
				esp2.add(new Especialidade("neurologia"));
				
				ArrayList<Especialidade> esp3 = new ArrayList<Especialidade>();
				esp3.add(new Especialidade("dermatologia"));
				
				// carregamentos dos telefones
				ArrayList<Telefone> tel1 = new ArrayList<Telefone>();
				tel1.add(new Telefone("98634-4543"));
				
				ArrayList<Telefone> tel2 = new ArrayList<Telefone>();
				tel2.add(new Telefone("98458-3432"));
				tel2.add(new Telefone("98796-9807"));
				
				ArrayList<Telefone> tel3 = new ArrayList<Telefone>();
				tel3.add(new Telefone("97434-3342"));
				
				ArrayList<Telefone> tel4 = new ArrayList<Telefone>();
				tel4.add(new Telefone("94573-4573"));
				
				ArrayList<Telefone> tel5 = new ArrayList<Telefone>();
				tel5.add(new Telefone("98764-6547"));
				
				ArrayList<Telefone> tel6 = new ArrayList<Telefone>();
				tel6.add(new Telefone("98765-8675"));
				
				ArrayList<Telefone> tel7 = new ArrayList<Telefone>();
				tel7.add(new Telefone("98798-8567"));
				
				ArrayList<Telefone> tel8 = new ArrayList<Telefone>();
				tel8.add(new Telefone("99643-4574"));
				
				// carregamento das enfermidades
				ArrayList<String> enf1 = new ArrayList<String>();
				enf1.add("dor de cabe�a");
				
				ArrayList<String> enf2 = new ArrayList<String>();
				enf2.add("diarr�ia");
				enf2.add("v�mito");
				
				ArrayList<String> enf3 = new ArrayList<String>();
				enf3.add("press�o alta");
				
				// carregamentos dos enderecos
				Endereco end1 = new Endereco("Jo�o Pessoa", "Banc�rios", "Antonia Martins dos Santos");
				Endereco end2 = new Endereco("Jo�o Pessoa", "Castelo Branco", "Ant�nio Camilo dos Santos");
				Endereco end3 = new Endereco("Jo�o Pessoa", "Banc�rios", "Banc�rio Ant�nio Jacinto de Souza");
				Endereco end4 = new Endereco("Jo�o Pessoa", "Mana�ra", "Aline Ferreira Ruffo");
				Endereco end5 = new Endereco("Jo�o Pessoa", "Mana�ra", "Carlos Alverga");
				Endereco end6 = new Endereco("Jo�o Pessoa", "Mana�ra", "Coronel Ant�nio Ferreira");
				Endereco end7 = new Endereco("Jo�o Pessoa", "Mangabeira", "Anna Soares de Lima");
				Endereco end8 = new Endereco("Jo�o Pessoa", "Mangabeira", "Ant�nio Carlos dos Santos Melo");
				
				try {
					// medicos
					Medico m1 = Fachada.cadastrarMedico("Everaldo Morais", "98776564531", "3635243-1", "UNIMED", end1, esp1, tel1);
					Medico m2 = Fachada.cadastrarMedico("Karla Ferraz", "87546768912", "34734-1", "SUS", end2, esp2, tel2);
					Fachada.cadastrarMedico("Jorge Moura", "98754326780", "56463-1", "SUS", end3, esp3, tel3);
					
					// pacientes
					Paciente p1 = Fachada.cadastrarPaciente("Fernanda Ara�jo", "87665435487", "12322-A", end4, enf1, tel4);
					Paciente p2 = Fachada.cadastrarPaciente("Vicente Pereira", "56473883564", "45343-B", end5, enf2, tel5);
					Fachada.cadastrarPaciente("Fernanda Gomes", "45364758691", "346543-A", end6, enf3, tel6);
					
					// responsaveis
					Fachada.cadastrarResponsavel("Jos� Da Silva", "45364758671", p1, "Pai", end7, tel7);
					Fachada.cadastrarResponsavel("Rafaela Martins", "34523467891", p2, "Tia", end8, tel8);
					
					// atendimentos
					Fachada.cadastrarAtendimento(m1, p1, new Date());
					Fachada.cadastrarAtendimento(m2, p2, new Date());
				}
				catch(Exception e) {}
			}
		}
		catch(Exception $e) {
			JOptionPane.showMessageDialog(null, $e.getMessage());
		}
		
		initialize();
	}

	private void initialize() {
		frmPrincipal = new JFrame();
		try {
			frmPrincipal.setContentPane(new Fundo("hospital.jpg"));
		} 
		catch (IOException e1) { }	

		frmPrincipal.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				Fachada.inicializar();
			}
			
			@Override
			public void windowActivated(WindowEvent e) { }
			
			@Override
			public void windowClosing(WindowEvent e) {
				Fachada.finalizar();
			}
		});
		frmPrincipal.setTitle("Hospital");
		frmPrincipal.setBounds(100, 100, 450, 300);
		frmPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPrincipal.getContentPane().setLayout(null);

		JMenuBar menuBar = new JMenuBar();
		frmPrincipal.setJMenuBar(menuBar);

		mnMedico = new JMenu("Medico");
		menuBar.add(mnMedico);

		medCadastro = new JMenuItem("Cadastrar");
		medCadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaCadastroMedico m = new TelaCadastroMedico();
				m.setVisible(true);
			}
		});
		mnMedico.add(medCadastro);
		
		medUpdate = new JMenuItem("Atualizar");
		medUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaAtualizacaoMedico m = new TelaAtualizacaoMedico();
				m.setVisible(true);
			}
		});
		mnMedico.add(medUpdate);
		
		medRemove = new JMenuItem("Remover");
		medRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaRemocaoMedico m = new TelaRemocaoMedico();
				m.setVisible(true);
			}
		});
		mnMedico.add(medRemove);
		
		medConsult1 = new JMenuItem("Procurar quantidade de atendimentos");
		medConsult1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaQuantidadeMedicoAtendimento m = new TelaQuantidadeMedicoAtendimento();
				m.setVisible(true);
			}
		});
		mnMedico.add(medConsult1);
		
		medConsult2 = new JMenuItem("Listar sem atendimentos");
		medConsult2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListagemMedicosSemAtendimento m = new TelaListagemMedicosSemAtendimento();
				m.setVisible(true);
			}
		});
		mnMedico.add(medConsult2);
		
		medConsult3 = new JMenuItem("Procurar por bairro");
		medConsult3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListagemMedicosPorBairro m = new TelaListagemMedicosPorBairro();
				m.setVisible(true);
			}
		});
		mnMedico.add(medConsult3);
		
		medConsult4 = new JMenuItem("Procurar com bairro igual");
		medConsult4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListagemMedicosBairroIgual m = new TelaListagemMedicosBairroIgual();
				m.setVisible(true);
			}
		});
		mnMedico.add(medConsult4);
		
		medConsult5 = new JMenuItem("Listar com v�rios telefones");
		medConsult5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListagemMedicosComVariosTelefones m = new TelaListagemMedicosComVariosTelefones();
				m.setVisible(true);
			}
		});
		mnMedico.add(medConsult5);
		
		medConsult6 = new JMenuItem("Listar todos");
		medConsult6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListagemMedico m = new TelaListagemMedico();
				m.setVisible(true);
			}
		});
		mnMedico.add(medConsult6);

		// Paciente
		mnPaciente = new JMenu("Paciente");
		menuBar.add(mnPaciente);

		paciCadastro = new JMenuItem("Cadastrar");
		paciCadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaCadastroPaciente p = new TelaCadastroPaciente();
				p.setVisible(true);
			}
		});
		mnPaciente.add(paciCadastro);
		
		paciUpdate = new JMenuItem("Atualizar");
		paciUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaAtualizacaoPaciente p = new TelaAtualizacaoPaciente();
				p.setVisible(true);
			}
		});
		mnPaciente.add(paciUpdate);
		
		paciConsult1 = new JMenuItem("Listar sem respons�vel");
		paciConsult1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListagemPacientesSemResponsaveis p = new TelaListagemPacientesSemResponsaveis();
				p.setVisible(true);
			}
		});
		mnPaciente.add(paciConsult1);
		
		paciConsult2 = new JMenuItem("Procurar com bairro igual");
		paciConsult2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListagemPacientePorBairro p = new TelaListagemPacientePorBairro();
				p.setVisible(true);
			}
		});
		mnPaciente.add(paciConsult2);
		
		paciConsult3 = new JMenuItem("Listar todos");
		paciConsult3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListagemPaciente p = new TelaListagemPaciente();
				p.setVisible(true);
			}
		});
		mnPaciente.add(paciConsult3);
		
		// Responsavel
		mnResponsavel = new JMenu("Responsavel");
		menuBar.add(mnResponsavel);

		respCadastro = new JMenuItem("Cadastrar");
		respCadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaCadastroResponsavel r = new TelaCadastroResponsavel();
				r.setVisible(true);
			}
		});
		mnResponsavel.add(respCadastro);
		
		respConsult1 = new JMenuItem("Listar todos");
		respConsult1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListagemResponsavel r = new TelaListagemResponsavel();
				r.setVisible(true);
			}
		});
		mnResponsavel.add(respConsult1);
		
		// Atendimento
		mnAtendimento = new JMenu("Atendimento");
		menuBar.add(mnAtendimento);

		atendCadastro = new JMenuItem("Cadastrar");
		atendCadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaCadastroAtendimento a = new TelaCadastroAtendimento();
				a.setVisible(true);
			}
		});
		mnAtendimento.add(atendCadastro);
		
		
		atendConsult1 = new JMenuItem("Listar pr�ximos");
		atendConsult1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListagemAtendimentoOrdenado a = new TelaListagemAtendimentoOrdenado();
				a.setVisible(true);
			}
		});
		mnAtendimento.add(atendConsult1);
	}
}
