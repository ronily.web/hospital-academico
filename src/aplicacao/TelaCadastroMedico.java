package aplicacao;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import fachada.Fachada;
import modelo.Endereco;
import modelo.Especialidade;
import modelo.Medico;
import modelo.Telefone;

public class TelaCadastroMedico extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField1, textField_2, textField_3, textField_4, textField_5, textField_6, textField_7, textField_8, textField_9, textField_10;
	private JLabel lblNome, lblCpf, lblCrm, lblOperadora, lblEspecialidade, lblTelefone, lblCidade, lblBairro, lblRua, lblNumero;
	private JButton btnCriar;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroMedico frame = new TelaCadastroMedico();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public TelaCadastroMedico() {
		setTitle("Cadastrar Medico");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 200, 280, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblNome = new JLabel("Nome");
		lblNome.setBounds(10, 14, 85, 14);
		contentPane.add(lblNome);

		lblCpf = new JLabel("Cpf");
		lblCpf.setBounds(10, 52, 85, 14);
		contentPane.add(lblCpf);
		
		lblCrm = new JLabel("Crm");
		lblCrm.setBounds(10, 83, 85, 14);
		contentPane.add(lblCrm);

		lblOperadora = new JLabel("Operadora");
		lblOperadora.setBounds(10, 114, 85, 14);
		contentPane.add(lblOperadora);
		
		lblEspecialidade = new JLabel("Especialidade");
		lblEspecialidade.setBounds(10, 145, 85, 14);
		contentPane.add(lblEspecialidade);
		
		lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(10, 176, 85, 14);
		contentPane.add(lblTelefone);
		
		lblNumero = new JLabel("Cidade");
		lblNumero.setBounds(10, 207, 85, 14);
		contentPane.add(lblNumero);
		
		lblCidade = new JLabel("Bairro");
		lblCidade.setBounds(10, 238, 85, 14);
		contentPane.add(lblCidade);
		
		lblBairro = new JLabel("Rua");
		lblBairro.setBounds(10, 269, 85, 14);
		contentPane.add(lblBairro);
		
		lblRua = new JLabel("Numero");
		lblRua.setBounds(10, 300, 85, 14);
		contentPane.add(lblRua);
		
		textField1 = new JTextField();
		textField1.setBounds(100, 18, 150, 20);
		contentPane.add(textField1);
		textField1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(100, 49, 150, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(100, 80, 150, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(100, 111, 150, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(100, 142, 150, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		textField_6 = new JTextField();
		textField_6.setBounds(100, 173, 150, 20);
		contentPane.add(textField_6);
		textField_6.setColumns(10);
		
		textField_7 = new JTextField();
		textField_7.setBounds(100, 204, 150, 20);
		contentPane.add(textField_7);
		textField_7.setColumns(10);
		
		textField_8 = new JTextField();
		textField_8.setBounds(100, 235, 150, 20);
		contentPane.add(textField_8);
		textField_8.setColumns(10);
		
		textField_9 = new JTextField();
		textField_9.setBounds(100, 266, 150, 20);
		contentPane.add(textField_9);
		textField_9.setColumns(10);
		
		textField_10 = new JTextField();
		textField_10.setBounds(100, 297, 150, 20);
		contentPane.add(textField_10);
		textField_10.setColumns(10);

		btnCriar = new JButton("Cadastrar");
		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					String nome = textField1.getText();
					String cpf = textField_2.getText();
					String crm = textField_3.getText();
					String operadora = textField_4.getText();
					String especialidade = textField_5.getText();
					String telefone = textField_6.getText();
					String cidade = textField_7.getText();
					String bairro = textField_8.getText();
					String rua = textField_9.getText();
					String numero = textField_10.getText();
					
					// phone
					ArrayList<Telefone> telefones = new ArrayList<Telefone>();
					telefones.add(new Telefone(telefone));
					
					// specialist
					ArrayList<Especialidade> especialidades = new ArrayList<Especialidade>();
					especialidades.add(new Especialidade(especialidade));
					
					// address
					Endereco end = new Endereco(cidade, bairro, rua);
					if(!numero.isEmpty()) end.setNumero(numero);
				
					// persist object
					Medico m = Fachada.cadastrarMedico(nome, cpf, crm, operadora, end, especialidades, telefones);
					JOptionPane.showMessageDialog(null, m.getNome() + " cadastrado");

					//clear inputs
					textField1.setText("");
					textField_2.setText("");
					textField_3.setText("");
					textField_4.setText("");
					textField_5.setText("");
					textField_6.setText("");
					textField_7.setText("");
					textField_8.setText("");
					textField_9.setText("");
					textField_10.setText("");
					textField1.requestFocus();
				}
				catch(Exception erro){
					JOptionPane.showMessageDialog(null, erro.getMessage());
				}
			}
		});
		btnCriar.setBounds(100, 330, 150, 23);
		contentPane.add(btnCriar);
	}
}

