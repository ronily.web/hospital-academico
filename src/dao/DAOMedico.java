package dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import modelo.Atendimento;
import modelo.Medico;

public class DAOMedico extends DAO<Medico> {
	
	@SuppressWarnings("unchecked")
	public List<Medico> findAll() {
		try {
			Query q = manager.createQuery("select m from Medico m "
					+ "JOIN FETCH m.telefones t "
					+ "JOIN FETCH m.especialidades e "
					+ "JOIN FETCH m.atendimentos a");
			return q.getResultList();
	
		}catch(NoResultException e){			
			return null;
		}
	}
	
	public Medico findByCrm (String n){
		try {
			Query q = manager.createQuery("select m from Medico m where m.crm= '" + n +"'");
			return (Medico) q.getSingleResult();
	
		}catch(NoResultException e){			
			return null;
		}
	}
	
	public Medico findByName (String nome){
		try {
			Query q = manager.createQuery("select m from Medico m where m.nome= '" + nome +"'");
			return (Medico) q.getSingleResult();

		}catch(NoResultException e){			
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findAllNames() {
		try {
			Query q = manager.createQuery("select m.nome from Medico m");
			return q.getResultList();
	
		}catch(NoResultException e){			
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Atendimento> findAttendance(String nome) {
		try {
			Query q = manager.createQuery("select a from Medico m JOIN m.atendimentos a where m.nome= '" + nome +"'");
			return q.getResultList();

		}catch(NoResultException e){			
			return null;
		}
	}
	
	public Long CountAttendanceOfDoctor(String nome) {
		try {
			Query q = manager.createQuery("select COUNT(a) from Medico m JOIN m.atendimentos a where m.nome= '" + nome +"'");
			return (Long) q.getSingleResult();
	
		}catch(NoResultException e){			
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Medico> findOfNeighborhood(String bairro) {
		try {
			Query q = manager.createQuery("select m from Medico m JOIN m.endereco e where e.bairro = '" + bairro +"'");
			return q.getResultList();

		}catch(NoResultException e){			
			return null;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<Medico> findOfNeighborhoodEqual(String nome) {
		try {
			Query q = manager.createQuery("select m from Medico m JOIN m.endereco e where m.nome != '" +nome+ "' AND e.bairro = ("
					+ "SELECT e.bairro FROM Medico m JOIN m.endereco e where m.nome = '" +nome+"')");
			return q.getResultList();

		}catch(NoResultException e){			
			return null;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<Medico> findNotAttendance() {
		try {
			Query q = manager.createQuery("SELECT m FROM Medico m WHERE SIZE(m.atendimentos) = 0");
			return q.getResultList();

		}catch(NoResultException e){			
			return null;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<Medico> findDoctorMultiplesPhones() {
		try {
			Query q = manager.createQuery("SELECT m FROM Medico m WHERE SIZE(m.telefones) > 1");
			return q.getResultList();

		}catch(NoResultException e){			
			return null;
		}		
	}
}
