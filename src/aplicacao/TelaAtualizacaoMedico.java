package aplicacao;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import fachada.Fachada;
import modelo.Medico;

public class TelaAtualizacaoMedico extends JFrame {
	List<String> medicos = Fachada.listarNomesDosMedicos();
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField1, textField_2, textField_3, textField_4, textField_5, textField_6, textField_7, textField_8;
	private JLabel lblNome, lblCpf, lblCrm, lblOperadora, lblMedicos;
	private JComboBox<String> cmbMedico;
	private JButton btnCriar;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaAtualizacaoMedico frame = new TelaAtualizacaoMedico();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public TelaAtualizacaoMedico() {
		try { 
			Medico med = Fachada.procurarMedicoPorNome(medicos.get(0));
			
			setTitle("Atualizar Medico");
			setResizable(false);
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 200, 280, 260);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			lblMedicos = new JLabel("Medicos");
			lblMedicos.setBounds(10, 14, 85, 14);
			contentPane.add(lblMedicos); 
			
			lblNome = new JLabel("Nome");
			lblNome.setBounds(10, 52, 85, 14);
			contentPane.add(lblNome);

			lblCpf = new JLabel("Cpf");
			lblCpf.setBounds(10, 83, 85, 14);
			contentPane.add(lblCpf);
			
			lblCrm = new JLabel("Crm");
			lblCrm.setBounds(10, 114, 85, 14);
			contentPane.add(lblCrm);

			lblOperadora = new JLabel("Operadora");
			lblOperadora.setBounds(10, 145, 85, 14);
			contentPane.add(lblOperadora);
			
			cmbMedico = new JComboBox<String>();
			for(String m: medicos) cmbMedico.addItem(m); // adiciona os medicos no comboBox
			cmbMedico.setSelectedIndex(0);
			cmbMedico.setEditable(false);
			cmbMedico.setBounds(100, 18, 150, 20);
	        contentPane.add(cmbMedico);
			
			textField1 = new JTextField();
			textField1.setBounds(100, 49, 150, 20);
			contentPane.add(textField1);
			textField1.setColumns(10);
			textField1.setText(med.getNome());

			textField_2 = new JTextField();
			textField_2.setBounds(100, 80, 150, 20);
			contentPane.add(textField_2);
			textField_2.setColumns(10);
			textField_2.setText(med.getCpf());

			textField_3 = new JTextField();
			textField_3.setBounds(100, 111, 150, 20);
			contentPane.add(textField_3);
			textField_3.setColumns(10);
			textField_3.setText(med.getCrm());
			
			textField_4 = new JTextField();
			textField_4.setBounds(100, 142, 150, 20);
			contentPane.add(textField_4);
			textField_4.setColumns(10);
			textField_4.setText(med.getOperadora());
		}
		catch(Exception erro) {}
		
		cmbMedico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Medico m = Fachada.procurarMedicoPorNome((String)cmbMedico.getSelectedItem());
					
					textField1.setText(m.getNome());
					textField_2.setText(m.getCpf());
					textField_3.setText(m.getCrm());
					textField_4.setText(m.getOperadora());
				}
				catch(Exception erro) { }
			}
		});

		btnCriar = new JButton("Atualizar");
		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					String nomeSelected = (String)cmbMedico.getSelectedItem();
					String nome = textField1.getText();
					String cpf = textField_2.getText();
					String crm = textField_3.getText();
					String operadora = textField_4.getText();
					
					// persist object
					Fachada.atualizarMedico(nomeSelected, nome, cpf, crm, operadora);
					JOptionPane.showMessageDialog(null, " atualizado");

					//clear inputs
					textField1.setText("");
					textField_2.setText("");
					textField_3.setText("");
					textField_4.setText("");
					textField_5.setText("");
					textField_6.setText("");
					textField_7.setText("");
					textField_8.setText("");
					medicos = Fachada.listarNomesDosMedicos();
					textField1.requestFocus();
					
					cmbMedico.removeAllItems();
					medicos = Fachada.listarNomesDosMedicos();
					for(String m: medicos) cmbMedico.addItem(m);
				}
				catch(Exception erro){
					JOptionPane.showMessageDialog(null, "erro");
				}
			}
		});
		btnCriar.setBounds(100, 175, 150, 23);
		contentPane.add(btnCriar);
	}
}

