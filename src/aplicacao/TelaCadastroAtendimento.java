package aplicacao;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import fachada.Fachada;
import modelo.Medico;
import modelo.Paciente;

public class TelaCadastroAtendimento extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JComboBox<String> cmbMedico, cmbPaciente;
	private JLabel lblMedico, lblPaciente, lblAtendimento;
	private JButton btnCriar;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroAtendimento frame = new TelaCadastroAtendimento();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public TelaCadastroAtendimento() {
		List<String> medicos = Fachada.listarNomesDosMedicos();
		List<String> pacientes = Fachada.listarNomesDosPacientes();
		
		setTitle("Cadastrar Atendimento");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 200, 280, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblMedico = new JLabel("Medico");
		lblMedico.setBounds(20, 21, 70, 14);
		contentPane.add(lblMedico);

		lblPaciente = new JLabel("Paciente");
		lblPaciente.setBounds(20, 52, 70, 14);
		contentPane.add(lblPaciente);
		
		lblAtendimento = new JLabel("Data");
		lblAtendimento.setBounds(20, 83, 70, 14);
		contentPane.add(lblAtendimento);
		
		cmbMedico = new JComboBox<String>();
		for(String m: medicos) cmbMedico.addItem(m); // adiciona os medicos no comboBox
		cmbMedico.setSelectedIndex(0);
		cmbMedico.setEditable(false);
		cmbMedico.setBounds(90, 18, 150, 20);
        contentPane.add(cmbMedico);
        
        cmbPaciente = new JComboBox<String>();
		for(String p: pacientes) cmbPaciente.addItem(p); // adiciona os pacientes no comboBox
		cmbPaciente.setSelectedIndex(0);
		cmbPaciente.setEditable(false);
		cmbPaciente.setBounds(90, 49, 150, 20);
        contentPane.add(cmbPaciente);

        textField = new JTextField();
        textField.setBounds(90, 80, 150, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		btnCriar = new JButton("Cadastrar");
		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					Medico medico = Fachada.procurarMedicoPorNome((String)cmbMedico.getSelectedItem());
					Paciente paciente = Fachada.procurarPacientePorNome((String)cmbPaciente.getSelectedItem());
					//DateFormat formatter = new SimpleDateFormat("MM/DD/YYYY").parse("01/29/2002");
					Date date = new SimpleDateFormat("DD/MM/yyyy").parse(textField.getText());
					
					Fachada.cadastrarAtendimento(medico, paciente, date);
					JOptionPane.showMessageDialog(null,"Atendimento cadastrado para: " + textField.getText());

					textField.setText("");
				}
				catch(Exception erro){
					JOptionPane.showMessageDialog(null, erro.getMessage());
				}
			}
		});
		btnCriar.setBounds(90, 120, 150, 23);
		contentPane.add(btnCriar);
	}
}

